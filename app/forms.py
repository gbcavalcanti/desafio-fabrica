from django.forms import ModelForm
from app.models import Pessoa, Diagnostico, Prevencao


class PessoaForm(ModelForm):
    class Meta:
        model = Pessoa
        fields = ['nome', 'tipo_sanguineo', 'CPF', 'data_de_nascimento', 'genero', 'data_de_cadastro', 'descricao_do_caso','gravidade_do_caso', 'sintomas', 'doenca']

class DiagnosticoForm(ModelForm):
    class Meta:
        model = Diagnostico
        fields = ['virus']

class PrevencaoForm(ModelForm):
    class Meta:
        model = Prevencao
        fields = ['virus', 'prevencao']
