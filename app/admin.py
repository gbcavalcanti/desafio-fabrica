from django.contrib import admin
from .models import Pessoa, Prevencao, Diagnostico




admin.site.register(Pessoa),
admin.site.register(Prevencao),
admin.site.register(Diagnostico),
