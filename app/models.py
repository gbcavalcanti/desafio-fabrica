from django.db import models
from multiselectfield import MultiSelectField

class Diagnostico(models.Model):
    virus = models.CharField(max_length=30, null=True, blank=True)

class Pessoa(models.Model):
    nome = models.CharField(max_length=30, null=True, blank=True)
    tipo_sanguineo = models.CharField(max_length=30, null=True, blank=True)
    CPF = models.CharField(max_length=30, null=True, blank=True)
    data_de_nascimento = models.DateField(null=True)
    GENERO_CHOICES = (
        ('Masculino', 'Masculino'),
        ('Feminino', 'Feminino'),
        ('Outro', 'Outro')
    )
    genero = models.CharField(max_length=30, choices=GENERO_CHOICES, null=True, blank=True)
    data_de_cadastro = models.DateField(null=True)
    descricao_do_caso = models.TextField(max_length=200, null=True, blank=True)
    MY_CHOICES = (
        ('leve', 'leve'),
        ('media', 'media'),
        ('grave', 'grave')
    )
    gravidade_do_caso = models.CharField(max_length=50, choices=MY_CHOICES, null=True, blank=True)
    SINTOMAS_CHOICES = (
        ('Dor de Cabeça', 'Dor de Cabeça'),
        ('Dores no Corpo', 'Dores no Corpo'),
        ('Coriza', 'Coriza'),
        ('Falta de ar', 'Falta de ar'),
        ('Tosse', 'Tosse'),
        ('Febre', 'Febre'),
        ('Dor na Garganta', 'Dor na Garganta'),
        ('Fadiga', 'Fadiga'),
        )
    sintomas = MultiSelectField(choices=SINTOMAS_CHOICES, null=True)
    doenca = models.OneToOneField(Diagnostico, max_length=50, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
      return self.nome

class Prevencao(models.Model):
    virus = models.CharField(max_length=50, null=True)
    prevencao = models.TextField(max_length=200, null=True, blank=True)
