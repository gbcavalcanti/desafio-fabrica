from django.urls import path
from app.views import create, list, update, delete, home, virus, updatevirus, listav, delete1, diagnostico


urlpatterns = [
    path('', home, name="home"),
    path('list/', list, name="list"),
    path('virus/', virus, name="virus"),
    path('create/', create, name="create"),
    path('listav/', listav, name="listav"),
    path('update/<int:id>/', update, name="update"),
    path('update1/<int:id>/', updatevirus, name="update1"),
    path('delete/<int:id>/', delete, name="delete"),
    path('delete1/<int:id>/', delete1, name="delete1"),
    path('diagnostico/', diagnostico, name="diagnostico")

]