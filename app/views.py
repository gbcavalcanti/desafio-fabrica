from django.shortcuts import render , redirect, get_object_or_404
from app.models import *
from app.forms import *
# Create your views here.
#create
#read
#update
#delete
def create(request):
    form = PessoaForm(request.POST) #request.post , eh informado no method do html
    if form.is_valid():
        form.save()
        return redirect('list')
    return render(request, 'create.html', {'form': form})

def home(request):
    return render(request, 'home.html')

def listav(request):
    virus = Prevencao.objects.all()
    return render(request, 'listav.html', {'virus': virus})

def virus(request):
    form = PrevencaoForm(request.POST)  # request.post , eh informado no method do html
    if form.is_valid():
        form.save()
        return redirect('listav')
    return render(request, 'virus.html', {'form': form})

def list(request):
    pessoa = Pessoa.objects.all()
    return render(request, 'list.html', {'pessoa': pessoa},)

def update(request, id):
    pessoa = get_object_or_404(Pessoa, pk=id)
    form = PessoaForm(request.POST or None, instance=pessoa)
    if form.is_valid():
        form.save()
        return redirect('list')
    return render(request, 'update.html', {'form': form})

def updatevirus(request, id):
    prevencao = get_object_or_404(Prevencao, pk=id)
    form = PrevencaoForm(request.POST or None, instance=prevencao)
    if form.is_valid():
        form.save()
        return redirect('list')
    return render(request, 'update1.html', {'form': form})

def delete(request, id):
    pessoa = get_object_or_404(Pessoa, pk=id)
    if request.method == 'POST':
        pessoa.delete()
        return redirect('list')
    return render(request, 'delete.html', {'pessoa': pessoa})

def delete1(request, id):
    prevencao = get_object_or_404(Prevencao, pk=id)
    if request.method == 'POST':
        prevencao.delete()
        return redirect('listav')
    return render(request, 'delete1.html', {'prevencao': prevencao})

def diagnostico(request):
    pessoa = Pessoa.objects.all()
    return render(request, 'diagnostico.html', {'pessoa': pessoa})

